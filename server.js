const express = require('express')
const app = express()

let todos = [
    {
        name: 'Kat',
        id: 59160730
    },
    {
        name: 'Kunlapat',
        id: 2
    }
]

//select * from
app.get('/todos',(req, res) => {
    res.send(todos)
})

app.post('/todos',(req, res) => {
    let newTodo = {
        name: 'Read a book',
        id: 3
    }

    todos.push(newTodo)
    res.status(201).send()
})

app.listen(3000, () => {
    console.log('TODO API Started at port 3000')
})

